package objektorientiert;

public class Ladungen {

	private String bezeichung;
	private int menge;
	
	
	public String getBezeichung() {
		return bezeichung;
	}
	public void setBezeichung(String bezeichung) {
		this.bezeichung = bezeichung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public Ladungen(String bezeichung, int menge) {
		super();
		this.bezeichung = bezeichung;
		this.menge = menge;
	}

	}


