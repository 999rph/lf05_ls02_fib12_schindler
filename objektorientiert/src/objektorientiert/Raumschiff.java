package objektorientiert;

import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystem;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladungen> ladungsverzeichnis = new ArrayList<Ladungen>();
	
    
    
    
    
    
    
    @Override
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
				+ energieversorgungInProzent + ", schildeInProzent=" + schildeInProzent + ", huelleInProzent="
				+ huelleInProzent + ", lebenserhaltungssystem=" + lebenserhaltungssystem + ", androidenAnzahl="
				+ androidenAnzahl + ", schiffsname=" + schiffsname + ", broadcastKommunikator=" + broadcastKommunikator
				+ ", ladungsverzeichnis=" + ladungsverzeichnis + "]";
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystem, int androidenAnzahl, String schiffsname,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladungen> ladungsverzeichnis) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystem = lebenserhaltungssystem;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}


	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystem() {
		return lebenserhaltungssystem;
	}
	public void setLebenserhaltungssystem(int lebenserhaltungssystem) {
		this.lebenserhaltungssystem = lebenserhaltungssystem;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	public ArrayList<Ladungen> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladungen> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	


	
	
	
	
	

	}
	
	

